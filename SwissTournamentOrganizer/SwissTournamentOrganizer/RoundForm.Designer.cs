﻿namespace SwissTournamentOrganizer
{
    partial class RoundForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.playerOneHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.playerTwoHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.roundCancelButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.playerOneHeader,
            this.playerTwoHeader});
            this.listView1.Location = new System.Drawing.Point(13, 13);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(897, 375);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // playerOneHeader
            // 
            this.playerOneHeader.Text = "Player One";
            this.playerOneHeader.Width = 188;
            // 
            // playerTwoHeader
            // 
            this.playerTwoHeader.Text = "Player Two";
            this.playerTwoHeader.Width = 200;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 395);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(273, 45);
            this.button1.TabIndex = 1;
            this.button1.Text = "Player One Wins 2 - 0 - 0";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(325, 394);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(273, 45);
            this.button2.TabIndex = 2;
            this.button2.Text = "Player One Wins 2 - 1 - 0";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(637, 395);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(273, 45);
            this.button3.TabIndex = 3;
            this.button3.Text = "Player One Wins 1 - 0 - 1";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(13, 447);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(273, 45);
            this.button4.TabIndex = 4;
            this.button4.Text = "Player Two Wins 2 - 0 - 0";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(325, 447);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(273, 45);
            this.button5.TabIndex = 5;
            this.button5.Text = "Player Two Wins 2 - 1 - 0";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(637, 447);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(273, 45);
            this.button6.TabIndex = 6;
            this.button6.Text = "Player Two Wins 1 - 0 - 1";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(13, 499);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(273, 45);
            this.button7.TabIndex = 7;
            this.button7.Text = "Players Drew 1 - 1 - 1";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // roundCancelButton
            // 
            this.roundCancelButton.Location = new System.Drawing.Point(637, 499);
            this.roundCancelButton.Name = "roundCancelButton";
            this.roundCancelButton.Size = new System.Drawing.Size(273, 45);
            this.roundCancelButton.TabIndex = 8;
            this.roundCancelButton.Text = "Cancel";
            this.roundCancelButton.UseVisualStyleBackColor = true;
            this.roundCancelButton.Click += new System.EventHandler(this.roundCancelButton_Click);
            // 
            // RoundForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(922, 575);
            this.Controls.Add(this.roundCancelButton);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.listView1);
            this.Name = "RoundForm";
            this.Text = "RoundForm";
            this.Load += new System.EventHandler(this.RoundForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader playerOneHeader;
        private System.Windows.Forms.ColumnHeader playerTwoHeader;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button roundCancelButton;
    }
}