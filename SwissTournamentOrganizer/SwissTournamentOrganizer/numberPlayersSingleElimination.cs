﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SwissTournamentOrganizer
{
	public partial class numberPlayersSingleElimination : Form
	{
		public int numberOfPlayers { get; set; }
		public numberPlayersSingleElimination()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			numberOfPlayers = int.Parse((string)comboBox1.SelectedItem);
			DialogResult = DialogResult.OK;
		}

		private void button2_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
		}
	}
}
