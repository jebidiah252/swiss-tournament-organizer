﻿namespace SwissTournamentOrganizer
{
    partial class TournamentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.waitingReportListView = new System.Windows.Forms.ListView();
            this.tableNumberHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.reportButton = new System.Windows.Forms.Button();
            this.dropPlayerButton = new System.Windows.Forms.Button();
            this.nextRoundButton = new System.Windows.Forms.Button();
            this.reportedListView = new System.Windows.Forms.ListView();
            this.reportedPlayersLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.roundNumberLabel = new System.Windows.Forms.Label();
            this.roundDetailViewButton = new System.Windows.Forms.Button();
            this.playerNameColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // waitingReportListView
            // 
            this.waitingReportListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.tableNumberHeader});
            this.waitingReportListView.Location = new System.Drawing.Point(13, 59);
            this.waitingReportListView.MultiSelect = false;
            this.waitingReportListView.Name = "waitingReportListView";
            this.waitingReportListView.Size = new System.Drawing.Size(399, 528);
            this.waitingReportListView.TabIndex = 0;
            this.waitingReportListView.UseCompatibleStateImageBehavior = false;
            this.waitingReportListView.View = System.Windows.Forms.View.Details;
            this.waitingReportListView.SelectedIndexChanged += new System.EventHandler(this.waitingReportListView_SelectedIndexChanged);
            // 
            // tableNumberHeader
            // 
            this.tableNumberHeader.Text = "Table Number";
            this.tableNumberHeader.Width = 154;
            // 
            // reportButton
            // 
            this.reportButton.Enabled = false;
            this.reportButton.Location = new System.Drawing.Point(418, 206);
            this.reportButton.Name = "reportButton";
            this.reportButton.Size = new System.Drawing.Size(421, 43);
            this.reportButton.TabIndex = 1;
            this.reportButton.Text = "Report Round";
            this.reportButton.UseVisualStyleBackColor = true;
            this.reportButton.Click += new System.EventHandler(this.reportButton_Click);
            // 
            // dropPlayerButton
            // 
            this.dropPlayerButton.Enabled = false;
            this.dropPlayerButton.Location = new System.Drawing.Point(418, 255);
            this.dropPlayerButton.Name = "dropPlayerButton";
            this.dropPlayerButton.Size = new System.Drawing.Size(421, 39);
            this.dropPlayerButton.TabIndex = 2;
            this.dropPlayerButton.Text = "Drop Player";
            this.dropPlayerButton.UseVisualStyleBackColor = true;
            // 
            // nextRoundButton
            // 
            this.nextRoundButton.Enabled = false;
            this.nextRoundButton.Location = new System.Drawing.Point(418, 546);
            this.nextRoundButton.Name = "nextRoundButton";
            this.nextRoundButton.Size = new System.Drawing.Size(227, 41);
            this.nextRoundButton.TabIndex = 3;
            this.nextRoundButton.Text = "Start Next Round";
            this.nextRoundButton.UseVisualStyleBackColor = true;
            this.nextRoundButton.Click += new System.EventHandler(this.nextRoundButton_Click);
            // 
            // reportedListView
            // 
            this.reportedListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.playerNameColumn});
            this.reportedListView.Location = new System.Drawing.Point(845, 59);
            this.reportedListView.Name = "reportedListView";
            this.reportedListView.Size = new System.Drawing.Size(433, 528);
            this.reportedListView.TabIndex = 4;
            this.reportedListView.UseCompatibleStateImageBehavior = false;
            this.reportedListView.View = System.Windows.Forms.View.Details;
            // 
            // reportedPlayersLabel
            // 
            this.reportedPlayersLabel.AutoSize = true;
            this.reportedPlayersLabel.Location = new System.Drawing.Point(845, 13);
            this.reportedPlayersLabel.Name = "reportedPlayersLabel";
            this.reportedPlayersLabel.Size = new System.Drawing.Size(178, 25);
            this.reportedPlayersLabel.TabIndex = 5;
            this.reportedPlayersLabel.Text = "Reported Players";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 25);
            this.label2.TabIndex = 6;
            this.label2.Text = "Waiting Report";
            // 
            // roundNumberLabel
            // 
            this.roundNumberLabel.AutoSize = true;
            this.roundNumberLabel.Location = new System.Drawing.Point(418, 13);
            this.roundNumberLabel.Name = "roundNumberLabel";
            this.roundNumberLabel.Size = new System.Drawing.Size(87, 25);
            this.roundNumberLabel.TabIndex = 7;
            this.roundNumberLabel.Text = "Round: ";
            // 
            // roundDetailViewButton
            // 
            this.roundDetailViewButton.Location = new System.Drawing.Point(652, 546);
            this.roundDetailViewButton.Name = "roundDetailViewButton";
            this.roundDetailViewButton.Size = new System.Drawing.Size(187, 41);
            this.roundDetailViewButton.TabIndex = 8;
            this.roundDetailViewButton.Text = "Detail View";
            this.roundDetailViewButton.UseVisualStyleBackColor = true;
            this.roundDetailViewButton.Click += new System.EventHandler(this.roundDetailViewButton_Click);
            // 
            // playerNameColumn
            // 
            this.playerNameColumn.Text = "Player Name";
            this.playerNameColumn.Width = 392;
            // 
            // TournamentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1290, 599);
            this.Controls.Add(this.roundDetailViewButton);
            this.Controls.Add(this.roundNumberLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.reportedPlayersLabel);
            this.Controls.Add(this.reportedListView);
            this.Controls.Add(this.nextRoundButton);
            this.Controls.Add(this.dropPlayerButton);
            this.Controls.Add(this.reportButton);
            this.Controls.Add(this.waitingReportListView);
            this.Name = "TournamentForm";
            this.Text = "TournamentForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView waitingReportListView;
        private System.Windows.Forms.Button reportButton;
        private System.Windows.Forms.Button dropPlayerButton;
        private System.Windows.Forms.Button nextRoundButton;
        private System.Windows.Forms.ListView reportedListView;
        private System.Windows.Forms.Label reportedPlayersLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label roundNumberLabel;
        private System.Windows.Forms.Button roundDetailViewButton;
        private System.Windows.Forms.ColumnHeader tableNumberHeader;
        private System.Windows.Forms.ColumnHeader playerNameColumn;
    }
}