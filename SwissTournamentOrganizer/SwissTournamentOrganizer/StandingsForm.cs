﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AppLayer.PlayerComponents;

namespace SwissTournamentOrganizer
{
	public partial class StandingsForm : Form
	{
		public StandingsForm()
		{
			InitializeComponent();
			standings = new List<Player>();
		}

		public List<Player> standings
		{
			get;
			set;
		}

		private void StandingsForm_Load(object sender, EventArgs e)
		{
			int i = 1;
			foreach (Player player in standings)
			{
				string[] row = {i.ToString(), player.FirstName + " " + player.LastName, player.PlayerDeck.DeckName, player.MatchPoints.ToString(), player.OpponentsMatchWinPercentage.ToString(), player.GameWinPercentage.ToString(), player.OpponentsGameWinPercentage.ToString() };

				ListViewItem item = new ListViewItem(row[0]);
				for(int j = 1; j < row.Length; j++)
				{
					item.SubItems.Add(row[j]);
				}
				listView1.Items.Add(item);
				i++;
			}
		}

		private void tournamentGeneratorButton_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.Yes;
		}

		private void exitButton_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
		}
	}
}
