﻿namespace SwissTournamentOrganizer
{
	partial class StandingsForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.listView1 = new System.Windows.Forms.ListView();
			this.rankColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.playerColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.MatchPoints = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.OpponentsMatchWinAverage = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.GamePointAverage = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.OppenentGameWinPercentage = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.deckColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.tournamentGeneratorButton = new System.Windows.Forms.Button();
			this.exitButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// listView1
			// 
			this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.rankColumn,
            this.playerColumn,
            this.deckColumn,
            this.MatchPoints,
            this.OpponentsMatchWinAverage,
            this.GamePointAverage,
            this.OppenentGameWinPercentage});
			this.listView1.Location = new System.Drawing.Point(6, 6);
			this.listView1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.listView1.Name = "listView1";
			this.listView1.Size = new System.Drawing.Size(789, 190);
			this.listView1.TabIndex = 0;
			this.listView1.UseCompatibleStateImageBehavior = false;
			this.listView1.View = System.Windows.Forms.View.Details;
			// 
			// rankColumn
			// 
			this.rankColumn.Text = "Rank";
			this.rankColumn.Width = 102;
			// 
			// playerColumn
			// 
			this.playerColumn.Text = "Player";
			this.playerColumn.Width = 98;
			// 
			// MatchPoints
			// 
			this.MatchPoints.DisplayIndex = 3;
			this.MatchPoints.Text = "MP";
			this.MatchPoints.Width = 90;
			// 
			// OpponentsMatchWinAverage
			// 
			this.OpponentsMatchWinAverage.DisplayIndex = 4;
			this.OpponentsMatchWinAverage.Text = "OMW";
			this.OpponentsMatchWinAverage.Width = 108;
			// 
			// GamePointAverage
			// 
			this.GamePointAverage.DisplayIndex = 5;
			this.GamePointAverage.Text = "GWA";
			this.GamePointAverage.Width = 104;
			// 
			// OppenentGameWinPercentage
			// 
			this.OppenentGameWinPercentage.DisplayIndex = 6;
			this.OppenentGameWinPercentage.Text = "OGWP";
			this.OppenentGameWinPercentage.Width = 124;
			// 
			// deckColumn
			// 
			this.deckColumn.DisplayIndex = 2;
			this.deckColumn.Text = "Deck Name";
			this.deckColumn.Width = 154;
			// 
			// tournamentGeneratorButton
			// 
			this.tournamentGeneratorButton.Location = new System.Drawing.Point(6, 199);
			this.tournamentGeneratorButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.tournamentGeneratorButton.Name = "tournamentGeneratorButton";
			this.tournamentGeneratorButton.Size = new System.Drawing.Size(113, 41);
			this.tournamentGeneratorButton.TabIndex = 1;
			this.tournamentGeneratorButton.Text = "Generate Tournament";
			this.tournamentGeneratorButton.UseVisualStyleBackColor = true;
			this.tournamentGeneratorButton.Click += new System.EventHandler(this.tournamentGeneratorButton_Click);
			// 
			// exitButton
			// 
			this.exitButton.Location = new System.Drawing.Point(682, 200);
			this.exitButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.exitButton.Name = "exitButton";
			this.exitButton.Size = new System.Drawing.Size(113, 41);
			this.exitButton.TabIndex = 2;
			this.exitButton.Text = "Exit";
			this.exitButton.UseVisualStyleBackColor = true;
			this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
			// 
			// StandingsForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(806, 246);
			this.Controls.Add(this.exitButton);
			this.Controls.Add(this.tournamentGeneratorButton);
			this.Controls.Add(this.listView1);
			this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.Name = "StandingsForm";
			this.Text = "StandingsForm";
			this.Load += new System.EventHandler(this.StandingsForm_Load);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListView listView1;
		private System.Windows.Forms.ColumnHeader rankColumn;
		private System.Windows.Forms.ColumnHeader playerColumn;
		private System.Windows.Forms.ColumnHeader deckColumn;
		private System.Windows.Forms.Button tournamentGeneratorButton;
		private System.Windows.Forms.Button exitButton;
		private System.Windows.Forms.ColumnHeader MatchPoints;
		private System.Windows.Forms.ColumnHeader OpponentsMatchWinAverage;
		private System.Windows.Forms.ColumnHeader GamePointAverage;
		private System.Windows.Forms.ColumnHeader OppenentGameWinPercentage;
	}
}