﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using AppLayer.PlayerComponents;
using System.Collections;
using AppLayer.FileIO;

namespace SwissTournamentOrganizer
{
    public partial class MainForm : Form
    {
        protected List<Player> currentContestants;
        public MainForm()
        {
            InitializeComponent();
            currentContestants = new List<Player>();
            ContestantListView.FullRowSelect = true;
        }

        private void AddPlayerButton_Click(object sender, EventArgs e)
        {
            var newPlayer = new NewPlayerForm()
            {
                FirstName = "Bob",
                LastName = "Dylan",
                DeckListPath = "C:/Desktop/mahSpoon.txt"
            };

            if(newPlayer.ShowDialog() == DialogResult.OK)
            {
                Player myPlayer = new Player(newPlayer.FirstName, newPlayer.LastName, newPlayer.DeckListPath);
                if (myPlayer.DeckFileName == null || myPlayer.PlayerDeck.MainDeck.Count < 60)
                    return;
                currentContestants.Add(myPlayer);
                string[] row = { myPlayer.FirstName, myPlayer.LastName, myPlayer.PlayerDeck.DeckName };
                ListViewItem myItem = new ListViewItem(row);
                ContestantListView.Items.Add(myItem);                
            }

			playersRegisteredLabel.Text = "Number of Players Registered: " + currentContestants.Count.ToString();
        }

        private void NewTournamentButton_Click(object sender, EventArgs e)
        {
            var myTournamentForm = new TournamentForm(currentContestants);
            myTournamentForm.Show();
        }

        private void ContestantListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(ContestantListView.SelectedIndices.Count == 1)
            {
                var hello = getItemsFromListViewControl();

                var otherOne = ContestantListView.SelectedIndices;

                mainDeckLabel.Text = "Main Deck for: " + System.Environment.NewLine + hello[otherOne[0] * 3] + " " + hello[(otherOne[0] * 3) + 1];
                sideboardLabel.Text = "Sideboard for: " + System.Environment.NewLine + hello[otherOne[0] * 3] + " " + hello[(otherOne[0] * 3) + 1];

                Player searchedFor = new Player();

                mainDeckListView.Items.Clear();
                sideboardListView.Items.Clear();

                foreach(var contestant in currentContestants)
                {
                    if (contestant.FirstName == (string)hello[(otherOne[0] * 3)] && contestant.LastName == (string)hello[(otherOne[0] * 3) + 1])
                    {
                        searchedFor = new Player((string)hello[(otherOne[0] * 3)], (string)hello[(otherOne[0] * 3) + 1], contestant.DeckFileName);
                    }
                }

                int count = 0;
                for(int i = 0; i < searchedFor.PlayerDeck.MainDeck.Count; i += count)
                {
                    if (searchedFor.PlayerDeck.MainDeck[i].CardName == "Island" ||
                        searchedFor.PlayerDeck.MainDeck[i].CardName == "Plains" ||
                        searchedFor.PlayerDeck.MainDeck[i].CardName == "Swamp" ||
                        searchedFor.PlayerDeck.MainDeck[i].CardName == "Mountain" ||
                        searchedFor.PlayerDeck.MainDeck[i].CardName == "Forest")
                    {
						count = 0;
                        for(int j = 0; j < searchedFor.PlayerDeck.MainDeck.Count; j++)
                        {
                            if(searchedFor.PlayerDeck.MainDeck[j].CardName != searchedFor.PlayerDeck.MainDeck[i].CardName)
                            {
                                continue;
                            }
                            else
                            {
                                count++;
                            }
                        }
                    }
                    else if(i + 1 <= searchedFor.PlayerDeck.MainDeck.Count - 1)
                    {
                        if (searchedFor.PlayerDeck.MainDeck[i].CardName == searchedFor.PlayerDeck.MainDeck[i + 1].CardName)
                        {
                            if (i + 2 <= searchedFor.PlayerDeck.MainDeck.Count - 1)
                            {
                                if (searchedFor.PlayerDeck.MainDeck[i].CardName == searchedFor.PlayerDeck.MainDeck[i + 2].CardName)
                                {
                                    if (i + 3 <= searchedFor.PlayerDeck.MainDeck.Count - 1)
                                    {
                                        if (searchedFor.PlayerDeck.MainDeck[i].CardName == searchedFor.PlayerDeck.MainDeck[i + 3].CardName)
                                        {
                                            count = 4;
                                        }
                                        else
                                        {
                                            count = 3;
                                        }
                                    }
                                }
                                else
                                {
                                    count = 2;
                                }
                            }
                        }
                        else
                        {
                            count = 1;
                        }
                    }

                    string[] row = { count.ToString(), searchedFor.PlayerDeck.MainDeck[i].CardName };
                    ListViewItem myItem = new ListViewItem(row);
                    mainDeckListView.Items.Add(myItem);
                }

                count = 0;
                for (int i = 0; i < searchedFor.PlayerDeck.Sideboard.Count; i += count)
                {
                    if (searchedFor.PlayerDeck.Sideboard[i].CardName == "Island" ||
                        searchedFor.PlayerDeck.Sideboard[i].CardName == "Plains" ||
                        searchedFor.PlayerDeck.Sideboard[i].CardName == "Swamp" ||
                        searchedFor.PlayerDeck.Sideboard[i].CardName == "Mountain" ||
                        searchedFor.PlayerDeck.Sideboard[i].CardName == "Forest")
                    {
                        for (int j = 0; j < searchedFor.PlayerDeck.Sideboard.Count; j++)
                        {
                            if (searchedFor.PlayerDeck.Sideboard[j].CardName != searchedFor.PlayerDeck.Sideboard[i].CardName)
                            {
                                break;
                            }
                            else
                            {
                                count++;
                            }
                        }
                    }
                    else if (i + 1 <= searchedFor.PlayerDeck.Sideboard.Count - 1)
                    {
                        if (searchedFor.PlayerDeck.Sideboard[i].CardName == searchedFor.PlayerDeck.Sideboard[i + 1].CardName)
                        {
                            if (i + 2 <= searchedFor.PlayerDeck.Sideboard.Count - 1)
                            {
                                if (searchedFor.PlayerDeck.Sideboard[i].CardName == searchedFor.PlayerDeck.Sideboard[i + 2].CardName)
                                {
                                    if (i + 3 <= searchedFor.PlayerDeck.Sideboard.Count - 1)
                                    {
                                        if (searchedFor.PlayerDeck.Sideboard[i].CardName == searchedFor.PlayerDeck.Sideboard[i + 3].CardName)
                                        {
                                            count = 4;
                                        }
                                        else
                                        {
                                            count = 3;
                                        }
                                    }
                                }
                                else
                                {
                                    count = 2;
                                }
                            }
                        }
                        else
                        {
                            count = 1;
                        }
                    }
					else
					{
						count = 1;
					}

                    string[] row = { count.ToString(), searchedFor.PlayerDeck.Sideboard[i].CardName };
                    ListViewItem myItem = new ListViewItem(row);
                    sideboardListView.Items.Add(myItem);
                }
            }
            else if (ContestantListView.SelectedIndices.Count == 0)
            {
                mainDeckLabel.Text = "Main Deck for: ";
                sideboardLabel.Text = "Sideboard for: ";
            }
        }

        private ArrayList getItemsFromListViewControl()
        {
            ArrayList lviItemsArrayList = new ArrayList();

            foreach (ListViewItem itemRow in ContestantListView.Items)
            {
                //lviItemsArrayList.Add(itemRow.Text); <-- Already included in SubItems so ... = )

                for (int i = 0; i < itemRow.SubItems.Count; i++)
                {
                    lviItemsArrayList.Add(itemRow.SubItems[i].Text);
                    // Do something useful here, for example the line above.
                }
            }
            return lviItemsArrayList;
        }

        private void DropPlayerButton_Click(object sender, EventArgs e)
        {
			var myItem = new ListViewItem();
            foreach(ListViewItem item in ContestantListView.SelectedItems)
            {
				myItem = item;
                ContestantListView.Items.Remove(item);
				string first = myItem.SubItems[0].Text;
				string last = myItem.SubItems[1].Text;
				string list = myItem.SubItems[2].Text;

				Player toRemove = new Player();
				foreach(Player player in currentContestants)
				{
					if (player.FirstName == first
						&& player.LastName == last
						&& player.PlayerDeck.DeckName == list)
					{
						toRemove = player;
					}
				}
				currentContestants.Remove(toRemove);
            }

            foreach(ListViewItem item in mainDeckListView.Items)
            {
                mainDeckListView.Items.Remove(item);
            }

            foreach (ListViewItem item in sideboardListView.Items)
            {
                sideboardListView.Items.Remove(item);
            }

            mainDeckLabel.Text = "Main Deck for: ";
            sideboardLabel.Text = "Sideboard for: ";
			playersRegisteredLabel.Text = "Number of Players Registered: " + currentContestants.Count.ToString();
        }

		private void csvReaderButton_Click(object sender, EventArgs e)
		{
			currentContestants.Clear();
			ContestantListView.Items.Clear();
			CSVReader myReader = new CSVReader();
			currentContestants = myReader.ReadCSV("exampleFile.csv");

			foreach(Player player in currentContestants)
			{
				string[] row = { player.FirstName, player.LastName, player.PlayerDeck.DeckName };
				ListViewItem myItem = new ListViewItem(row);
				ContestantListView.Items.Add(myItem);
			}
			playersRegisteredLabel.Text = "Number of Players Registered: " + currentContestants.Count.ToString();
		}
	}
}
