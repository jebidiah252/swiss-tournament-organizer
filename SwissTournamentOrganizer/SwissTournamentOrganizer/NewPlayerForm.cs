﻿using System;
using System.Windows.Forms;

namespace SwissTournamentOrganizer
{
    public partial class NewPlayerForm : Form
    {
        public NewPlayerForm()
        {
            InitializeComponent();
        }

        public string FirstName
        {
            get
            {
                return FirstNameTextBox.Text;
            }
            set
            {
                FirstNameTextBox.Text = value;
            }
        }

        public string LastName
        {
            get
            {
                return LastNameTextBox.Text;
            }
            set
            {
                LastNameTextBox.Text = value;
            }
        }

        public string DeckListPath
        {
            get
            {
                return DeckListTextBox.Text;
            }
            set
            {
                DeckListTextBox.Text = value;
            }
        }

        private void BrowseButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if(dialog.ShowDialog() == DialogResult.OK)
            {
                DeckListTextBox.Text = dialog.FileName;
                DeckListPath = dialog.FileName;
            }
        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
