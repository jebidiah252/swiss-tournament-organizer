﻿namespace SwissTournamentOrganizer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.ContestantListView = new System.Windows.Forms.ListView();
			this.FirstNameHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.LastNameHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.DeckNameHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.AddPlayerButton = new System.Windows.Forms.Button();
			this.DropPlayerButton = new System.Windows.Forms.Button();
			this.NewTournamentButton = new System.Windows.Forms.Button();
			this.mainDeckListView = new System.Windows.Forms.ListView();
			this.copiesColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.cardNameColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.mainDeckLabel = new System.Windows.Forms.Label();
			this.sideboardListView = new System.Windows.Forms.ListView();
			this.qtyColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.nameOfCardColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.sideboardLabel = new System.Windows.Forms.Label();
			this.csvReaderButton = new System.Windows.Forms.Button();
			this.playersRegisteredLabel = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// ContestantListView
			// 
			this.ContestantListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.FirstNameHeader,
            this.LastNameHeader,
            this.DeckNameHeader});
			this.ContestantListView.Location = new System.Drawing.Point(12, 13);
			this.ContestantListView.Margin = new System.Windows.Forms.Padding(4);
			this.ContestantListView.Name = "ContestantListView";
			this.ContestantListView.Size = new System.Drawing.Size(856, 356);
			this.ContestantListView.TabIndex = 0;
			this.ContestantListView.UseCompatibleStateImageBehavior = false;
			this.ContestantListView.View = System.Windows.Forms.View.Details;
			this.ContestantListView.SelectedIndexChanged += new System.EventHandler(this.ContestantListView_SelectedIndexChanged);
			// 
			// FirstNameHeader
			// 
			this.FirstNameHeader.Text = "First Name";
			this.FirstNameHeader.Width = 128;
			// 
			// LastNameHeader
			// 
			this.LastNameHeader.Text = "Last Name";
			this.LastNameHeader.Width = 146;
			// 
			// DeckNameHeader
			// 
			this.DeckNameHeader.Text = "Deck Name";
			this.DeckNameHeader.Width = 147;
			// 
			// AddPlayerButton
			// 
			this.AddPlayerButton.Location = new System.Drawing.Point(12, 562);
			this.AddPlayerButton.Margin = new System.Windows.Forms.Padding(4);
			this.AddPlayerButton.Name = "AddPlayerButton";
			this.AddPlayerButton.Size = new System.Drawing.Size(192, 44);
			this.AddPlayerButton.TabIndex = 1;
			this.AddPlayerButton.Text = "Add Player";
			this.AddPlayerButton.UseVisualStyleBackColor = true;
			this.AddPlayerButton.Click += new System.EventHandler(this.AddPlayerButton_Click);
			// 
			// DropPlayerButton
			// 
			this.DropPlayerButton.Location = new System.Drawing.Point(252, 562);
			this.DropPlayerButton.Margin = new System.Windows.Forms.Padding(4);
			this.DropPlayerButton.Name = "DropPlayerButton";
			this.DropPlayerButton.Size = new System.Drawing.Size(156, 44);
			this.DropPlayerButton.TabIndex = 2;
			this.DropPlayerButton.Text = "Drop Player";
			this.DropPlayerButton.UseVisualStyleBackColor = true;
			this.DropPlayerButton.Click += new System.EventHandler(this.DropPlayerButton_Click);
			// 
			// NewTournamentButton
			// 
			this.NewTournamentButton.Location = new System.Drawing.Point(462, 562);
			this.NewTournamentButton.Margin = new System.Windows.Forms.Padding(6);
			this.NewTournamentButton.Name = "NewTournamentButton";
			this.NewTournamentButton.Size = new System.Drawing.Size(244, 44);
			this.NewTournamentButton.TabIndex = 3;
			this.NewTournamentButton.Text = "Start New Tournament";
			this.NewTournamentButton.UseVisualStyleBackColor = true;
			this.NewTournamentButton.Click += new System.EventHandler(this.NewTournamentButton_Click);
			// 
			// mainDeckListView
			// 
			this.mainDeckListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.copiesColumn,
            this.cardNameColumn});
			this.mainDeckListView.Location = new System.Drawing.Point(1164, 13);
			this.mainDeckListView.Margin = new System.Windows.Forms.Padding(6);
			this.mainDeckListView.Name = "mainDeckListView";
			this.mainDeckListView.Size = new System.Drawing.Size(484, 356);
			this.mainDeckListView.TabIndex = 4;
			this.mainDeckListView.UseCompatibleStateImageBehavior = false;
			this.mainDeckListView.View = System.Windows.Forms.View.Details;
			// 
			// copiesColumn
			// 
			this.copiesColumn.Text = "Qty.";
			// 
			// cardNameColumn
			// 
			this.cardNameColumn.Text = "Card Name";
			this.cardNameColumn.Width = 177;
			// 
			// mainDeckLabel
			// 
			this.mainDeckLabel.AutoSize = true;
			this.mainDeckLabel.Location = new System.Drawing.Point(992, 13);
			this.mainDeckLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
			this.mainDeckLabel.Name = "mainDeckLabel";
			this.mainDeckLabel.Size = new System.Drawing.Size(157, 25);
			this.mainDeckLabel.TabIndex = 5;
			this.mainDeckLabel.Text = "Main Deck for: ";
			// 
			// sideboardListView
			// 
			this.sideboardListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.qtyColumn,
            this.nameOfCardColumn});
			this.sideboardListView.Location = new System.Drawing.Point(1164, 387);
			this.sideboardListView.Margin = new System.Windows.Forms.Padding(6);
			this.sideboardListView.Name = "sideboardListView";
			this.sideboardListView.Size = new System.Drawing.Size(484, 216);
			this.sideboardListView.TabIndex = 6;
			this.sideboardListView.UseCompatibleStateImageBehavior = false;
			this.sideboardListView.View = System.Windows.Forms.View.Details;
			// 
			// qtyColumn
			// 
			this.qtyColumn.Text = "Qty.";
			this.qtyColumn.Width = 69;
			// 
			// nameOfCardColumn
			// 
			this.nameOfCardColumn.Text = "Card Name";
			this.nameOfCardColumn.Width = 169;
			// 
			// sideboardLabel
			// 
			this.sideboardLabel.AutoSize = true;
			this.sideboardLabel.Location = new System.Drawing.Point(998, 387);
			this.sideboardLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
			this.sideboardLabel.Name = "sideboardLabel";
			this.sideboardLabel.Size = new System.Drawing.Size(147, 25);
			this.sideboardLabel.TabIndex = 7;
			this.sideboardLabel.Text = "Sideboard for:";
			// 
			// csvReaderButton
			// 
			this.csvReaderButton.Location = new System.Drawing.Point(749, 562);
			this.csvReaderButton.Name = "csvReaderButton";
			this.csvReaderButton.Size = new System.Drawing.Size(164, 44);
			this.csvReaderButton.TabIndex = 8;
			this.csvReaderButton.Text = "Read CSV";
			this.csvReaderButton.UseVisualStyleBackColor = true;
			this.csvReaderButton.Click += new System.EventHandler(this.csvReaderButton_Click);
			// 
			// playersRegisteredLabel
			// 
			this.playersRegisteredLabel.AutoSize = true;
			this.playersRegisteredLabel.Location = new System.Drawing.Point(13, 387);
			this.playersRegisteredLabel.Name = "playersRegisteredLabel";
			this.playersRegisteredLabel.Size = new System.Drawing.Size(311, 25);
			this.playersRegisteredLabel.TabIndex = 9;
			this.playersRegisteredLabel.Text = "Number of Players Registered: ";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1676, 619);
			this.Controls.Add(this.playersRegisteredLabel);
			this.Controls.Add(this.csvReaderButton);
			this.Controls.Add(this.sideboardLabel);
			this.Controls.Add(this.sideboardListView);
			this.Controls.Add(this.mainDeckLabel);
			this.Controls.Add(this.mainDeckListView);
			this.Controls.Add(this.NewTournamentButton);
			this.Controls.Add(this.DropPlayerButton);
			this.Controls.Add(this.AddPlayerButton);
			this.Controls.Add(this.ContestantListView);
			this.Margin = new System.Windows.Forms.Padding(4);
			this.Name = "MainForm";
			this.Text = "Swiss Tournament Organizer";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView ContestantListView;
        private System.Windows.Forms.Button AddPlayerButton;
        private System.Windows.Forms.Button DropPlayerButton;
        private System.Windows.Forms.ColumnHeader FirstNameHeader;
        private System.Windows.Forms.ColumnHeader LastNameHeader;
        private System.Windows.Forms.ColumnHeader DeckNameHeader;
        private System.Windows.Forms.Button NewTournamentButton;
        private System.Windows.Forms.ListView mainDeckListView;
        private System.Windows.Forms.Label mainDeckLabel;
        private System.Windows.Forms.ColumnHeader copiesColumn;
        private System.Windows.Forms.ColumnHeader cardNameColumn;
        private System.Windows.Forms.ListView sideboardListView;
        private System.Windows.Forms.ColumnHeader qtyColumn;
        private System.Windows.Forms.ColumnHeader nameOfCardColumn;
        private System.Windows.Forms.Label sideboardLabel;
		private System.Windows.Forms.Button csvReaderButton;
		private System.Windows.Forms.Label playersRegisteredLabel;
	}
}

