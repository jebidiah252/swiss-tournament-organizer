﻿namespace SwissTournamentOrganizer
{
    partial class NewPlayerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.label1 = new System.Windows.Forms.Label();
			this.FirstNameTextBox = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.LastNameTextBox = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.DeckListTextBox = new System.Windows.Forms.TextBox();
			this.SubmitButton = new System.Windows.Forms.Button();
			this.MyCancelButton = new System.Windows.Forms.Button();
			this.BrowseButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 10);
			this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(57, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "First Name";
			// 
			// FirstNameTextBox
			// 
			this.FirstNameTextBox.Location = new System.Drawing.Point(68, 7);
			this.FirstNameTextBox.Margin = new System.Windows.Forms.Padding(2);
			this.FirstNameTextBox.Name = "FirstNameTextBox";
			this.FirstNameTextBox.Size = new System.Drawing.Size(150, 20);
			this.FirstNameTextBox.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(6, 38);
			this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(58, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "Last Name";
			// 
			// LastNameTextBox
			// 
			this.LastNameTextBox.Location = new System.Drawing.Point(68, 35);
			this.LastNameTextBox.Margin = new System.Windows.Forms.Padding(2);
			this.LastNameTextBox.Name = "LastNameTextBox";
			this.LastNameTextBox.Size = new System.Drawing.Size(150, 20);
			this.LastNameTextBox.TabIndex = 3;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(6, 71);
			this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(52, 13);
			this.label3.TabIndex = 4;
			this.label3.Text = "Deck List";
			// 
			// DeckListTextBox
			// 
			this.DeckListTextBox.Location = new System.Drawing.Point(68, 68);
			this.DeckListTextBox.Margin = new System.Windows.Forms.Padding(2);
			this.DeckListTextBox.Name = "DeckListTextBox";
			this.DeckListTextBox.Size = new System.Drawing.Size(150, 20);
			this.DeckListTextBox.TabIndex = 5;
			// 
			// SubmitButton
			// 
			this.SubmitButton.Location = new System.Drawing.Point(14, 97);
			this.SubmitButton.Margin = new System.Windows.Forms.Padding(2);
			this.SubmitButton.Name = "SubmitButton";
			this.SubmitButton.Size = new System.Drawing.Size(52, 24);
			this.SubmitButton.TabIndex = 6;
			this.SubmitButton.Text = "Submit";
			this.SubmitButton.UseVisualStyleBackColor = true;
			this.SubmitButton.Click += new System.EventHandler(this.SubmitButton_Click);
			// 
			// MyCancelButton
			// 
			this.MyCancelButton.Location = new System.Drawing.Point(259, 97);
			this.MyCancelButton.Margin = new System.Windows.Forms.Padding(2);
			this.MyCancelButton.Name = "MyCancelButton";
			this.MyCancelButton.Size = new System.Drawing.Size(52, 24);
			this.MyCancelButton.TabIndex = 7;
			this.MyCancelButton.Text = "Cancel";
			this.MyCancelButton.UseVisualStyleBackColor = true;
			this.MyCancelButton.Click += new System.EventHandler(this.CancelButton_Click);
			// 
			// BrowseButton
			// 
			this.BrowseButton.Location = new System.Drawing.Point(224, 66);
			this.BrowseButton.Name = "BrowseButton";
			this.BrowseButton.Size = new System.Drawing.Size(87, 23);
			this.BrowseButton.TabIndex = 8;
			this.BrowseButton.Text = "Browse";
			this.BrowseButton.UseVisualStyleBackColor = true;
			this.BrowseButton.Click += new System.EventHandler(this.BrowseButton_Click);
			// 
			// NewPlayerForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(323, 127);
			this.Controls.Add(this.BrowseButton);
			this.Controls.Add(this.MyCancelButton);
			this.Controls.Add(this.SubmitButton);
			this.Controls.Add(this.DeckListTextBox);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.LastNameTextBox);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.FirstNameTextBox);
			this.Controls.Add(this.label1);
			this.Margin = new System.Windows.Forms.Padding(2);
			this.Name = "NewPlayerForm";
			this.Text = "Add Player";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FirstNameTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox LastNameTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox DeckListTextBox;
        private System.Windows.Forms.Button SubmitButton;
        private System.Windows.Forms.Button MyCancelButton;
        private System.Windows.Forms.Button BrowseButton;
    }
}