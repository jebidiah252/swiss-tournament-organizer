﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using AppLayer.PlayerComponents;
using AppLayer.TournamentComponents;
using System.Collections;
using AppLayer.TieBreakers;

namespace SwissTournamentOrganizer
{
    public partial class TournamentForm : Form
    {
        private Tournament newTournament;
        private Player playerOne, playerTwo;
        public TournamentForm()
        {
            InitializeComponent();
        }

        public void startRound()
        {
            newTournament.groupTables();
            LoadTables();
        }

        public TournamentForm(List<Player> contestants)
        {
            InitializeComponent();
            newTournament = new Tournament(contestants);
            
            roundNumberLabel.Text = "Round: " + newTournament.currentRound + " of " + newTournament.Rounds;
            startRound();  
        }

        private void LoadTables()
        {
            waitingReportListView.Items.Clear();
            for(int i = 1; i <= newTournament.Tables.Count; i++)
            {
                waitingReportListView.Items.Add(i.ToString());
            }
        }

        private void roundDetailViewButton_Click(object sender, EventArgs e)
        {
            var myDetailView = new RoundDetailViewForm(newTournament);
            myDetailView.Show();
        }

        private void nextRoundButton_Click(object sender, EventArgs e)
        {
			startRound();
			if (newTournament.currentRound > newTournament.Rounds)
			{
				List<Player> standings = newTournament.CalculateTieBreaks();

				var standingsReporter = new StandingsForm
				{
					standings = standings
				};

				DialogResult dr = standingsReporter.ShowDialog();

				if (dr == DialogResult.Yes)
				{
					// This generates the tournament bracket
					var finalsTime = new numberPlayersSingleElimination();
					var mehSpoon = finalsTime.ShowDialog();
					if (mehSpoon == DialogResult.OK)
					{
						var singleElimination = new SingleEliminationForm
						{
							players = standings,
							numberOfPlayers = finalsTime.numberOfPlayers
						};
						singleElimination.Show();
					}
					else
					{

					}
					Close();
				}
				else
				{
					standingsReporter.Close();
				}
				return;
			}
            roundNumberLabel.Text = "Round: " + newTournament.currentRound + " of " + newTournament.Rounds;
            reportedListView.Items.Clear();
			nextRoundButton.Enabled = false;

			if(newTournament.currentRound == newTournament.Rounds)
			{
				nextRoundButton.Text = "Show Standings";
			}
        }

		private void waitingReportListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            var listing = getItemsFromListViewControl();
            if(waitingReportListView.SelectedIndices.Count == 1)
            {
                reportButton.Enabled = true;
                var other = waitingReportListView.SelectedIndices;
                playerOne = newTournament.Tables[other[0]].firstPlayer;
                playerTwo = newTournament.Tables[other[0]].secondPlayer;
            }
            else
            {
                reportButton.Enabled = false;
            }
        }

        private ArrayList getItemsFromListViewControl()
        {
            ArrayList lviItemsArrayList = new ArrayList();

            foreach (ListViewItem itemRow in waitingReportListView.Items)
            {
                //lviItemsArrayList.Add(itemRow.Text); <-- Already included in SubItems so ... = )

                for (int i = 0; i < itemRow.SubItems.Count; i++)
                {
                    lviItemsArrayList.Add(itemRow.SubItems[i].Text);
                    // Do something useful here, for example the line above.
                }
            }
            return lviItemsArrayList;
        }

        // KEY FOR BUTTONS TO HELP WITH REPORTING
        // Assuming Player one is on the left
        // BUTTON 1: 2-0-0 (ABORT)
        // BUTTON 2: 2-1-0 (IGNORE)
        // BUTTON 3: 1-0-1 (NO)
        // BUTTON 4: 0-2-0 (NONE)
        // BUTTON 5: 1-2-0 (OK)
        // BUTTON 6: 0-1-1 (RETRY)
        // BUTTON 7: 1-1-1 (YES)

        private void reportButton_Click(object sender, EventArgs e)
        {
            var roundReporter = new RoundForm
            {
                FirstPlayer = playerOne,
                SecondPlayer = playerTwo
            };

            var otherIndex = waitingReportListView.SelectedIndices;

            int theIndex;
			int theNewIndex;

            DialogResult dr = roundReporter.ShowDialog();

            switch (dr)
            {
                case DialogResult.Abort:
                    theIndex = newTournament.Contestants.IndexOf(playerOne);
					theNewIndex = newTournament.Contestants.IndexOf(playerTwo);
					newTournament.AssignWinsToPlayer(
						newTournament, 
						theIndex, 
						theNewIndex, 
						3, 
						6, 
						0
					);
					waitingReportListView.Items.RemoveAt(otherIndex[0]);
					reportedListView.Items.Add(playerOne.FirstName + " " + playerOne.LastName);
					reportedListView.Items.Add(playerTwo.FirstName + " " + playerTwo.LastName);
					break;
                case DialogResult.Ignore:
                    theIndex = newTournament.Contestants.IndexOf(playerOne);
                    theNewIndex = newTournament.Contestants.IndexOf(playerTwo);
					newTournament.AssignWinsToPlayer(
						newTournament,
						theIndex,
						theNewIndex,
						3,
						6,
						3
					);
					waitingReportListView.Items.RemoveAt(otherIndex[0]);
					reportedListView.Items.Add(playerOne.FirstName + " " + playerOne.LastName);
					reportedListView.Items.Add(playerTwo.FirstName + " " + playerTwo.LastName);
					break;
                case DialogResult.No:
                    theIndex = newTournament.Contestants.IndexOf(playerOne);
					theNewIndex = newTournament.Contestants.IndexOf(playerTwo);
					newTournament.AssignWinsToPlayer(
						newTournament,
						theIndex,
						theNewIndex,
						3,
						4,
						1
					);
					waitingReportListView.Items.RemoveAt(otherIndex[0]);
					reportedListView.Items.Add(playerOne.FirstName + " " + playerOne.LastName);
					reportedListView.Items.Add(playerTwo.FirstName + " " + playerTwo.LastName);
					break;
                case DialogResult.None:
                    theIndex = newTournament.Contestants.IndexOf(playerTwo);
					theNewIndex = newTournament.Contestants.IndexOf(playerOne);
					newTournament.AssignWinsToPlayer(
						newTournament,
						theIndex,
						theNewIndex,
						3,
						6,
						0
					);
					waitingReportListView.Items.RemoveAt(otherIndex[0]);
                    reportedListView.Items.Add(playerOne.FirstName + " " + playerOne.LastName);
                    reportedListView.Items.Add(playerTwo.FirstName + " " + playerTwo.LastName);
					break;
                case DialogResult.OK:
                    theIndex = newTournament.Contestants.IndexOf(playerTwo);
					theNewIndex = newTournament.Contestants.IndexOf(playerOne);
					newTournament.AssignWinsToPlayer(
						newTournament,
						theIndex,
						theNewIndex,
						3,
						6,
						3
					);
					waitingReportListView.Items.RemoveAt(otherIndex[0]);
                    reportedListView.Items.Add(playerOne.FirstName + " " + playerOne.LastName);
                    reportedListView.Items.Add(playerTwo.FirstName + " " + playerTwo.LastName);
                    break;
                case DialogResult.Retry:
                    theIndex = newTournament.Contestants.IndexOf(playerTwo);
					theNewIndex = newTournament.Contestants.IndexOf(playerOne);
					newTournament.AssignWinsToPlayer(
						newTournament,
						theIndex,
						theNewIndex,
						3,
						4,
						1
					);
                    waitingReportListView.Items.RemoveAt(otherIndex[0]);
                    reportedListView.Items.Add(playerOne.FirstName + " " + playerOne.LastName);
                    reportedListView.Items.Add(playerTwo.FirstName + " " + playerTwo.LastName);
                    break;
                case DialogResult.Yes:
                    theIndex = newTournament.Contestants.IndexOf(playerOne);
					theNewIndex = newTournament.Contestants.IndexOf(playerTwo);			
					newTournament.AssignWinsToPlayer(
						newTournament,
						theIndex,
						theNewIndex,
						1,
						4,
						4
					);
                    waitingReportListView.Items.RemoveAt(otherIndex[0]);
                    reportedListView.Items.Add(playerOne.FirstName + " " + playerOne.LastName);
                    reportedListView.Items.Add(playerTwo.FirstName + " " + playerTwo.LastName);
                    break;
                default:
                    break;
            }

            if (waitingReportListView.Items.Count == 0)
            {
                nextRoundButton.Enabled = true;
            }
        }
    }
}
