﻿namespace SwissTournamentOrganizer
{
	partial class SingleEliminationForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tablesView = new System.Windows.Forms.ListView();
			this.tableHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.playerRankHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.reportedPlayersList = new System.Windows.Forms.ListView();
			this.playerHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.nextRoundButton = new System.Windows.Forms.Button();
			this.tableReportButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// tablesView
			// 
			this.tablesView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.tableHeader,
            this.playerRankHeader});
			this.tablesView.Location = new System.Drawing.Point(13, 13);
			this.tablesView.Name = "tablesView";
			this.tablesView.Size = new System.Drawing.Size(480, 596);
			this.tablesView.TabIndex = 0;
			this.tablesView.UseCompatibleStateImageBehavior = false;
			this.tablesView.View = System.Windows.Forms.View.Details;
			this.tablesView.SelectedIndexChanged += new System.EventHandler(this.tablesView_SelectedIndexChanged);
			// 
			// tableHeader
			// 
			this.tableHeader.Text = "Table";
			this.tableHeader.Width = 136;
			// 
			// playerRankHeader
			// 
			this.playerRankHeader.Text = "Ranks of Players";
			this.playerRankHeader.Width = 210;
			// 
			// reportedPlayersList
			// 
			this.reportedPlayersList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.playerHeader});
			this.reportedPlayersList.Location = new System.Drawing.Point(759, 13);
			this.reportedPlayersList.Name = "reportedPlayersList";
			this.reportedPlayersList.Size = new System.Drawing.Size(379, 596);
			this.reportedPlayersList.TabIndex = 1;
			this.reportedPlayersList.UseCompatibleStateImageBehavior = false;
			this.reportedPlayersList.View = System.Windows.Forms.View.Details;
			// 
			// playerHeader
			// 
			this.playerHeader.Text = "Player";
			this.playerHeader.Width = 336;
			// 
			// nextRoundButton
			// 
			this.nextRoundButton.Location = new System.Drawing.Point(500, 554);
			this.nextRoundButton.Name = "nextRoundButton";
			this.nextRoundButton.Size = new System.Drawing.Size(253, 54);
			this.nextRoundButton.TabIndex = 2;
			this.nextRoundButton.Text = "Next Round";
			this.nextRoundButton.UseVisualStyleBackColor = true;
			this.nextRoundButton.Click += new System.EventHandler(this.nextRoundButton_Click);
			// 
			// tableReportButton
			// 
			this.tableReportButton.Enabled = false;
			this.tableReportButton.Location = new System.Drawing.Point(500, 208);
			this.tableReportButton.Name = "tableReportButton";
			this.tableReportButton.Size = new System.Drawing.Size(253, 53);
			this.tableReportButton.TabIndex = 3;
			this.tableReportButton.Text = "Report Table";
			this.tableReportButton.UseVisualStyleBackColor = true;
			this.tableReportButton.Click += new System.EventHandler(this.tableReportButton_Click);
			// 
			// SingleEliminationForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1150, 621);
			this.Controls.Add(this.tableReportButton);
			this.Controls.Add(this.nextRoundButton);
			this.Controls.Add(this.reportedPlayersList);
			this.Controls.Add(this.tablesView);
			this.Name = "SingleEliminationForm";
			this.Text = "SingleEliminationForm";
			this.Load += new System.EventHandler(this.SingleEliminationForm_Load);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListView tablesView;
		private System.Windows.Forms.ListView reportedPlayersList;
		private System.Windows.Forms.ColumnHeader tableHeader;
		private System.Windows.Forms.ColumnHeader playerRankHeader;
		private System.Windows.Forms.ColumnHeader playerHeader;
		private System.Windows.Forms.Button nextRoundButton;
		private System.Windows.Forms.Button tableReportButton;
	}
}