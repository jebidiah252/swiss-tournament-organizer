﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using AppLayer.PlayerComponents;
using AppLayer.TournamentComponents;
using System.IO;
using System.Collections;

namespace SwissTournamentOrganizer
{
	public partial class SingleEliminationForm : Form
	{
		public List<Player> players { get; set; }
		public int numberOfPlayers { get; set; }
		private List<Table> finalTables;
		private int roundNumber = 0;
		private Player playerOne, playerTwo;
		private List<int> ranksToProceed;
		public SingleEliminationForm()
		{
			InitializeComponent();
			players = new List<Player>();
			finalTables = new List<Table>();
			ranksToProceed = new List<int>();
		}

		private void SingleEliminationForm_Load(object sender, EventArgs e)
		{
			CreateNextRound(numberOfPlayers.ToString() + "Round" + roundNumber + ".txt");
			LoadTables();
		}

		private void LoadTables()
		{
			tablesView.Items.Clear();
			for (int i = 1; i <= finalTables.Count; i++)
			{
				string rankOne = finalTables[i - 1].firstPlayer.Rank.ToString();
				string rankTwo = finalTables[i - 1].secondPlayer.Rank.ToString();

				ListViewItem item = new ListViewItem(i.ToString());
				item.SubItems.Add(rankOne + ", " + rankTwo);
				tablesView.Items.Add(item);
			}
		}

		private void CreateNextRound(string filename)
		{
			try
			{
				StreamReader reader = new StreamReader(filename);
				string lineOne, lineTwo;
				while ((lineOne = reader.ReadLine()) != null)
				{
					lineTwo = reader.ReadLine();

					int rankOne = int.Parse(lineOne);
					int rankTwo = int.Parse(lineTwo);

					Player playerOne = new Player();
					Player playerTwo = new Player();

					for (int i = 0; i < players.Count; i++)
					{
						if (players[i].Rank == rankOne)
						{
							playerOne = players[i];
						}
						else if (players[i].Rank == rankTwo)
						{
							playerTwo = players[i];
						}
					}

					finalTables.Add(new Table(playerOne, playerTwo));
				}
			}
			catch(Exception e)
			{
				Console.Write(e.Data);
			}
		}

		// KEY FOR BUTTONS TO HELP WITH REPORTING
		// Assuming Player one is on the left
		// BUTTON 1: 2-0-0 (ABORT)
		// BUTTON 2: 2-1-0 (IGNORE)
		// BUTTON 3: 1-0-1 (NO)
		// BUTTON 4: 0-2-0 (NONE)
		// BUTTON 5: 1-2-0 (OK)
		// BUTTON 6: 0-1-1 (RETRY)
		// BUTTON 7: 1-1-1 (YES)

		private void tableReportButton_Click(object sender, EventArgs e)
		{
			// this guy is for reporting who won.
			var round = new RoundForm
			{
				FirstPlayer = playerOne,
				SecondPlayer = playerTwo
			};

			DialogResult dr = round.ShowDialog();

			var otherIndex = tablesView.SelectedIndices;

			switch (dr)
			{
				case DialogResult.Abort:
					ranksToProceed.Add(playerOne.Rank);
					tablesView.Items.RemoveAt(otherIndex[0]);
					reportedPlayersList.Items.Add(playerOne.FirstName + " " + playerOne.LastName);
					break;
				case DialogResult.Ignore:
					ranksToProceed.Add(playerOne.Rank);
					tablesView.Items.RemoveAt(otherIndex[0]);
					reportedPlayersList.Items.Add(playerOne.FirstName + " " + playerOne.LastName);
					break;
				case DialogResult.No:
					ranksToProceed.Add(playerOne.Rank);
					tablesView.Items.RemoveAt(otherIndex[0]);
					reportedPlayersList.Items.Add(playerOne.FirstName + " " + playerOne.LastName);
					break;
				case DialogResult.None:
					ranksToProceed.Add(playerTwo.Rank);
					tablesView.Items.RemoveAt(otherIndex[0]);
					reportedPlayersList.Items.Add(playerTwo.FirstName + " " + playerTwo.LastName);
					break;
				case DialogResult.OK:
					ranksToProceed.Add(playerTwo.Rank);
					tablesView.Items.RemoveAt(otherIndex[0]);
					reportedPlayersList.Items.Add(playerTwo.FirstName + " " + playerTwo.LastName);
					break;
				case DialogResult.Retry:
					ranksToProceed.Add(playerTwo.Rank);
					tablesView.Items.RemoveAt(otherIndex[0]);
					reportedPlayersList.Items.Add(playerTwo.FirstName + " " + playerTwo.LastName);
					break;
				case DialogResult.Yes:
					break;
			}
		}

		private void tablesView_SelectedIndexChanged(object sender, EventArgs e)
		{
			// this guy is for changing which players you're looking at.
			var listing = getItemsFromListViewControl();
			if (tablesView.SelectedIndices.Count == 1)
			{
				tableReportButton.Enabled = true;
				var other = tablesView.SelectedIndices;

				playerOne = finalTables[other[0]].firstPlayer;
				playerTwo = finalTables[other[0]].secondPlayer;
			}
			else
			{
				tableReportButton.Enabled = false;
			}
		}

		private void nextRoundButton_Click(object sender, EventArgs e)
		{
			roundNumber++;
			ranksToProceed.Reverse();
			finalTables.Clear();
			tablesView.Items.Clear();
			reportedPlayersList.Items.Clear();
			List<string> myLines = new List<string>();
			foreach(int number in ranksToProceed)
			{
				myLines.Add(number.ToString());
			}

			if(myLines.Count == 1)
			{
				// display the winner and the standings
				string winnerFirstName = "";
				string winnerLastName = "";

				foreach(Player player in players)
				{
					if (player.Rank == int.Parse(myLines[0]))
					{
						winnerFirstName = player.FirstName;
						winnerLastName = player.LastName;
					}
				}
				MessageBox.Show(winnerFirstName + " " + winnerLastName + " is the winner!");
				Close();
				return;
			}
			string[] lines = myLines.ToArray();
			File.WriteAllLines(numberOfPlayers.ToString() + "Round" + roundNumber + ".txt", lines);
			CreateNextRound(numberOfPlayers.ToString() + "Round" + roundNumber + ".txt");
			LoadTables();
			try
			{
				File.Delete(numberOfPlayers.ToString() + "Round" + roundNumber + ".txt");
			}
			catch(Exception ex)
			{
				Console.Write(ex.Data);
			}
			ranksToProceed.Clear();

			if(finalTables.Count == 1)
			{
				// change the next round button to display winner and final standings
			}
		}

		private ArrayList getItemsFromListViewControl()
		{
			ArrayList lviItemsArrayList = new ArrayList();

			foreach (ListViewItem itemRow in tablesView.Items)
			{
				//lviItemsArrayList.Add(itemRow.Text); <-- Already included in SubItems so ... = )

				for (int i = 0; i < itemRow.SubItems.Count; i++)
				{
					lviItemsArrayList.Add(itemRow.SubItems[i].Text);
					// Do something useful here, for example the line above.
				}
			}
			return lviItemsArrayList;
		}
	}
}
