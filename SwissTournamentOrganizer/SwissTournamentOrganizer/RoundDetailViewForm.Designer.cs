﻿namespace SwissTournamentOrganizer
{
    partial class RoundDetailViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.tableNumberHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.firstPlayerHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.secondPlayerHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.tableNumberHeader,
            this.firstPlayerHeader,
            this.secondPlayerHeader});
            this.listView1.Location = new System.Drawing.Point(13, 13);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(1353, 584);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // tableNumberHeader
            // 
            this.tableNumberHeader.Text = "Table Number";
            this.tableNumberHeader.Width = 164;
            // 
            // firstPlayerHeader
            // 
            this.firstPlayerHeader.Text = "Player One";
            this.firstPlayerHeader.Width = 294;
            // 
            // secondPlayerHeader
            // 
            this.secondPlayerHeader.Text = "Player Two";
            this.secondPlayerHeader.Width = 322;
            // 
            // RoundDetailViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1378, 609);
            this.Controls.Add(this.listView1);
            this.Name = "RoundDetailViewForm";
            this.Text = "RoundDetialViewForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader tableNumberHeader;
        private System.Windows.Forms.ColumnHeader firstPlayerHeader;
        private System.Windows.Forms.ColumnHeader secondPlayerHeader;
    }
}