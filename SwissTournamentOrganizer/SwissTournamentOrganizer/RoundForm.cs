﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AppLayer.PlayerComponents;

// KEY FOR BUTTONS TO HELP WITH REPORTING
// Assuming Player one is on the left
// BUTTON 1: 2-0-0 (ABORT)
// BUTTON 2: 2-1-0 (IGNORE)
// BUTTON 3: 1-0-1 (NO)
// BUTTON 4: 0-2-0 (NONE)
// BUTTON 5: 1-2-0 (OK)
// BUTTON 6: 0-1-1 (RETRY)
// BUTTON 7: 1-1-1 (YES)

namespace SwissTournamentOrganizer
{
    public partial class RoundForm : Form
    {
        public RoundForm()
        {
            InitializeComponent();
            
        }

        public Player FirstPlayer
        {
            get;
            set;
        }

        public Player SecondPlayer
        {
            get;
            set;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Player One Wins 2 - 0 - 0
            DialogResult = DialogResult.Abort;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // Player One Wins 2 - 1 - 0
            DialogResult = DialogResult.Ignore;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // Player One Wins 1 - 0 - 1
            DialogResult = DialogResult.No;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            // Player Two Wins 2 - 0 - 0
            DialogResult = DialogResult.None;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            // Player Two Wins 2 - 1 - 0
            DialogResult = DialogResult.OK;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            // Player Two Wins 1 - 0 - 1
            DialogResult = DialogResult.Retry;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            // Player Drew Wins 1 - 1 - 1
            DialogResult = DialogResult.Yes;
        }

        private void roundCancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void RoundForm_Load(object sender, EventArgs e)
        {
            string[] row = { FirstPlayer.FirstName + " " + FirstPlayer.LastName, SecondPlayer.FirstName + " " + SecondPlayer.LastName };
            ListViewItem item = new ListViewItem(row);
            listView1.Items.Add(item);
        }
    }
}
