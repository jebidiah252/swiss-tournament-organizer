﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AppLayer.TournamentComponents;

namespace SwissTournamentOrganizer
{
    public partial class RoundDetailViewForm : Form
    {
        public RoundDetailViewForm()
        {
            InitializeComponent();
        }

        public RoundDetailViewForm(Tournament myTournament)
        {
            InitializeComponent();
            foreach(Table table in myTournament.Tables)
            {
                string[] row = { table.tableNumber.ToString(), table.firstPlayer.FirstName + " " + table.firstPlayer.LastName, table.secondPlayer.FirstName + " " + table.secondPlayer.LastName };
                ListViewItem item = new ListViewItem(row);
                listView1.Items.Add(item);
            }
        }
    }
}
