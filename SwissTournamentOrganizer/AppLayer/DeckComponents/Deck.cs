﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppLayer.FileIO;

namespace AppLayer.DeckComponents
{
    public class Deck
    {
        public string DeckName { get; set; }
        public List<Card> MainDeck;
        public List<Card> Sideboard;

        public Deck()
        {
            MainDeck = new List<Card>();
            Sideboard = new List<Card>();
        }
    }
}
