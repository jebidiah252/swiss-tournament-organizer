﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLayer.DeckComponents
{
    public class Card
    {
        public Card(string cardName)
        {
            CardName = cardName;
        }
        public string CardName { get; set; }
        public string ConvertedManaCost { get; set; }
        public string FlavorText { get; set; }
        public string Color { get; set; }
    }
}
