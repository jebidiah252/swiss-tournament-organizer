﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using AppLayer.DeckComponents;

namespace AppLayer.FileIO
{
    public class TextReader
    {
		// This class is not needed for reading in an entire deck
		// for a magic tournament. It is not required for the 
		// Jarvis implementation.
        public Deck ReadInDeck(string filename)
        {
            Deck myDeck = new Deck();

            try
            {
                StreamReader reader = new StreamReader(filename);
                string line;
                line = reader.ReadLine();
                myDeck.DeckName = line;

                while ((line = reader.ReadLine()) != null)
                {
                    if (line == "Main Deck")
                    {
                        while ((line = reader.ReadLine()) != "")
                        {
                            string[] copiesAndNames = line.Split(',');
                            int copies = int.Parse(copiesAndNames[0]);

                            for (int i = 0; i < copies; i++)
                            {
                                myDeck.MainDeck.Add(new Card(copiesAndNames[1]));
                            }
                        }
                    }
                    else if (line == "Sideboard")
                    {
                        while ((line = reader.ReadLine()) != null)
                        {
                            string[] copiesAndNames = line.Split(',');
                            int copies = int.Parse(copiesAndNames[0]);

                            for (int i = 0; i < copies; i++)
                            {
                                myDeck.Sideboard.Add(new Card(copiesAndNames[1]));
                            }
                        }
                    }
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            

            return myDeck;
        }
    }
}
