﻿using System;
using System.Collections.Generic;
using System.IO;
using AppLayer.PlayerComponents;

namespace AppLayer.FileIO
{
	// This class is for the benefit of laziness. Jarvis does not 
	// need this class. It's for the benefit of a TO to load up
	// an entire tournament in one fell swoop.
    public class CSVReader
    {
		public List<Player> ReadCSV(string filename)
		{
			List<Player> results = new List<Player>();

			try
			{
				StreamReader reader = new StreamReader(filename);
				string[] line;

				while((line = reader.ReadLine().Split(',')) != null)
				{
					Player myPlayer = new Player(line[0], line[1], line[2]);
					results.Add(myPlayer);
				}
			}
			catch(Exception e)
			{
				Console.Write(e.Data);
			}

			return results;
		}
    }
}
