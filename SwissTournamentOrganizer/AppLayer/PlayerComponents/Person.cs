﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLayer.PlayerComponents
{
	// This is the base for a player in a tournament. 
	// More information can be added to this if needed.

    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public Person(string first, string last)
        {
            FirstName = first;
            LastName = last;
        }

        public Person()
        {

        }
    }
}
