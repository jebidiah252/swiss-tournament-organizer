﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppLayer.DeckComponents;
using AppLayer.FileIO;

namespace AppLayer.PlayerComponents
{
	// This is needed for the tournament. There are some members that 
	// are not needed for the Jarvis implementation like DCI Number
	// and Player Deck, but the numerical members and opponents are
	// important for running the tournament and the tie breakers.
    public class Player : Person
    {
        public Deck PlayerDeck;
        public string DCINumber;
        public string DeckFileName;
        public int Byes;
        public int MatchPoints;
        public float MatchWinPercentage;
        public float OpponentsMatchWinPercentage;
        public int GamePoints;
        public float GameWinPercentage;
		public int GamesPlayed;
        public float OpponentsGameWinPercentage;
        public List<Player> Opponents;
		public bool hasTable;
		public int Rank;

        public Player(string first, string last, string deckFileName) : base(first, last)
        {
            TextReader reader = new TextReader();
            DeckFileName = deckFileName;
            PlayerDeck = reader.ReadInDeck(DeckFileName);
            Opponents = new List<Player>();
        }

        public Player() : base()
        {
			TextReader reader = new TextReader();
			PlayerDeck = new Deck();
			Opponents = new List<Player>();
        }
    }
}
