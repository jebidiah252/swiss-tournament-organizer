﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppLayer.PlayerComponents;

namespace AppLayer.TournamentComponents
{
	// This class is important for the magic implementation of
	// the tournament for determining who the two people who are
	// playing each other. This 
    public class Table
    {
        public Player firstPlayer { get; set; }
        public Player secondPlayer { get; set; }
        public int tableNumber { get; set; }

        public Table(Player playerOne, Player playerTwo)
        {
            firstPlayer = playerOne;
            secondPlayer = playerTwo;
        }
    }
}
